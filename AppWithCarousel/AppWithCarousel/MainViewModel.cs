﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace AppWithCarousel
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<ContentView> _itemViews;
        public ObservableCollection<ContentView> ItemViews
        {
            get => _itemViews;
            set => SetValue(ref _itemViews, value);
        }

        public MainViewModel()
        {
            ItemViews = new ObservableCollection<ContentView>(new ContentView[] 
            {
                new View1(),
                new View2(),
                new View3(),
                new View4()
            });
        }
    }

}
