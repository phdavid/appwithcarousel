﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AppWithCarousel
{
    public class TemplateSelector : DataTemplateSelector
    {
        public DataTemplate View1Templ { get; set; }
        public DataTemplate View2Templ { get; set; }
        public DataTemplate View3Templ { get; set; }
        public DataTemplate View4Templ { get; set; }

        public TemplateSelector()
        {
            View1Templ = new DataTemplate(typeof(View1));
            View2Templ = new DataTemplate(typeof(View2));
            View3Templ = new DataTemplate(typeof(View3));
            View4Templ = new DataTemplate(typeof(View4));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is ContentView vw)
            {
                switch (vw)
                { 
                    case View1 vw1:
                        return View1Templ;
                    case View2 vw2:
                        return View2Templ;
                    case View3 vw3:
                        return View3Templ;
                    case View4 vw4:
                        return View4Templ;
                    default:
                        return View1Templ;
                }
            }
            return View1Templ;
        }
    }
}
